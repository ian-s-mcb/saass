"""Contains functionality for graph creation and segmentation
"""
import networkx as nx
import numpy as np
import scipy as sp

from . import common


def create_weighted_graph(data, grid_graph, regional_model, boundary_model,
                          regional_params, boundary_params, training_labels=None):
    """Creates a weighted graph based off an image

    `regional_params` and `boundary_params` are specific to the provided model
    types. They may contain weight, sigma, gamma, and eps.

    Parameters
    ----------
    data : np.array
        The image being segmented.
    grid_graph : NetworkX graph
        A graph based off the shape of data that only contains grid info (ie.
        pixel neighbors) and no weights.
    regional_model : dict
    boundary_model : dict
    regional_params : dict
    boundary_params : dict
    training_labels : np.array, optional
        Class labeling from a previous segmentation, which was either
        created manually with an image editor or create automatically
        though a first pass with SAASS. The array has two axes that
        specify a pixel location. The values in the array are label values
        (0 for bg, 1 for fg). This argument is only provided when
        segmenting a training image. Non-training images won't have masks
        available.

    Returns
    -------
    weighted_graph : NetworkX graph
    all_params : dict
        Contains all the user-specified model types and regional/boundary
        parameters. This dict makes it convenient to identify the results when
        the weighted graph is eventually cut.
    """
    # Check that model has been trainined (if it supports training)
    if 'model' in boundary_model and not boundary_model['is_trained']:
        raise Exception('Boundary model supports training, '
                        'but has not been trained yet')

    # Cast data as float
    # Needed because boundary_energy_func_trained() misbehaves when given unsigned
    # integer data.
    data = data.astype(np.float)

    # Organize regional specs (type, weight, parameters)
    r_energy_type = regional_model['model_type']
    r_weight = regional_params['weight']
    r_energy_params_bg = {'mu'    : regional_model['bg']['mean'],
                          'sigma' : regional_model['bg']['std'],
                          'gamma' : regional_params['gamma']}
    r_energy_params_fg = {'mu'    : regional_model['fg']['mean'],
                          'sigma' : regional_model['fg']['std'],
                          'gamma' : regional_params['gamma']}

    # Organize boundary type, weight
    b_energy_type = boundary_model['model_type']
    b_weight = boundary_params['weight']

    # Modify a copy of the boundary parameters. This is needed because the
    # energy functions don't expect 'weight' as a parameter.
    b_energy_params = dict(boundary_params)
    del b_energy_params['weight']

    # Choose costs that either force or prevent cuts from being made
    max_nocut_cost = 10 ** (10)
    min_cut_cost = 10 ** (-10)

    # Get bg, fg nodes in masks (if masks are provided)
    if training_labels is not None:
        backnodes = set(zip(*np.where(training_labels == 0)))
        forenodes = set(zip(*np.where(training_labels == 1.0)))
    else:
        backnodes = forenodes = set()

    # Compute regional weights for bg nodes without mask info
    weights_bg = \
        [(node, 't', r_weight * regional_energy(data[node], r_energy_type, r_energy_params_bg))
        for node in grid_graph.nodes()
        if node not in backnodes and
           node not in forenodes]

    # Compute regional weights for fg nodes without mask info
    weights_fg = \
        [('s', node, r_weight * regional_energy(data[node], r_energy_type, r_energy_params_fg))
        for node in grid_graph.nodes()
        if node not in backnodes and
           node not in forenodes]

    # Compute regional weights for bg, fg nodes with mask info
    weights_bg += [(node, "t", min_cut_cost)   for node in backnodes]
    weights_bg += [(node, "t", max_nocut_cost) for node in forenodes]
    weights_fg += [('s', node, max_nocut_cost) for node in backnodes]
    weights_fg += [('s', node, min_cut_cost)   for node in forenodes]

    # Compute boundary weights
    # Trainable and untrainable boundary models are treated differently
    if 'model' in boundary_model:
        edges = grid_graph.edges()
        energies = boundary_energy_func_trained(data, edges, boundary_model['model'], boundary_model['extracted_feature_type'])
        xi, xj = zip(*edges)
        weights_bound = zip(xi, xj, b_weight * energies)
    else:
        weights_bound = \
            [(xi, xj, b_weight * boundary_energy(data[xi], data[xj], b_energy_type, b_energy_params))
            for xi, xj in grid_graph.edges()]

    # Add weights to a new graph
    weighted_graph = nx.Graph()
    weighted_graph.add_weighted_edges_from(weights_bg)
    weighted_graph.add_weighted_edges_from(weights_fg)
    weighted_graph.add_weighted_edges_from(weights_bound)

    # Group all parameters for convenience
    all_params = group_all_params(regional_model, boundary_model,
                                  regional_params, boundary_params)

    return (weighted_graph, all_params)


def regional_energy(x, r_energy_type, r_energy_params):
    """Computes regional energy for a particular energy type

    Parameters
    ----------
    x : float
        Intensity at one pixel.
    r_energy_type : {'normal'}
    r_energy_params : dict
        Parameters for the energy function corresponding to specified energy
        type.

    Returns
    -------
    energy : int or float
    """
    energy_func = {'normal': regional_energy_funct_normal}

    if r_energy_type not in energy_func:
        raise Exception('Unsupported regional energy type: {}'.format(r_energy_type))

    return energy_func[r_energy_type](x, **r_energy_params)


def regional_energy_funct_normal(x, mu, sigma=1, gamma=2):
    """Computes an energy using a normal distribution

    Parameters
    ----------
    x : float
        Intensity at one pixel.
    mu : float
        Mean from the regional model.
    sigma : float
        Standard deviation from the regional model.
    gamma : float
        Exponent. When `gamma` equals 2, energy is the negative log of the
        normal distribution.

    Returns
    -------
    """
    return np.abs((x - mu) / sigma) ** gamma


def boundary_energy(xi, xj, b_energy_type, b_energy_params):
    """Computes boundary energy for a particular energy type

    This function is only called for non-trainable boundary energy types, like
    for example 'exponential'.

    Parameters
    ----------
    xi, xj : float
        Data at two neightboring pixels of an image.
    b_energy_type : {'constant', 'exponential', 'inverse', 'sigmoidal'}
    b_energy_params : dict
        Parameters for the energy function corresponding to specified energy
        type.

    Returns
    -------
    energy : float
    """
    energy_func = {'constant': boundary_energy_func_constant,
                   'exponential': boundary_energy_func_exponential,
                   'inverse': boundary_energy_func_inverse,
                   'sigmoidal': boundary_energy_func_sigmoidial}

    if b_energy_type not in energy_func:
        raise Exception('Unsupported regional energy type: {}'.format(b_energy_type))

    u = np.abs(1.0 * xi - xj)

    return energy_func[b_energy_type](u, **b_energy_params)


def boundary_energy_func_constant():
    """Computes a constant energy

    Constant = 1
    """
    return 1


def boundary_energy_func_exponential(u, sigma, gamma):
    """Computes an exponentional energy

    .. math::

        \mbox{ExpE}(u,\sigma,\gamma) = \mbox{exp}(-|u/\sigma|^\gamma)

    Parameters
    ----------
    u : float
        Difference in intensity between two neighboring pixels.
    sigma : float
        Standard deviation.
    gamma : float
        Exponent.

    Returns
    -------
    energy : float
    """
    return np.exp(-(u / sigma) ** gamma)


def boundary_energy_func_inverse(u, gamma, eps):
    """Computes an inverse power energy

    .. math::

        \mbox{InvE}(u,\gamma,\epsilon) = |u + \epsilon |^{-\gamma}

    Parameters
    ----------
    u : float
        Difference in intensity between two neighboring pixels.
    gamma : float
        The power that `u` is raise to.
    eps : float
        A small positive number to keep the energy from exploding.

    Returns
    -------
    energy : float
    """
    return (u + eps) ** (-gamma)


def boundary_energy_func_sigmoidial(u, sigma, gamma, eps):
    """Computes a sigmoidal energy

    In this sigmoidal, `u` is `1 + eps` at zero, `sigma` is the value where 1
    starts dropping, `gamma` is the value had dropped to `eps`, `eps` is a
    constant cost added to everything.

    Parameters
    ----------
    u : float
        Difference in intensity between two neighboring pixels.
    sigma : float
    gamma : float
    eps : float

    Returns
    -------
    energy : float
    """
    return sp.special.expit(5 - (u - sigma) * (gamma - sigma) * 10) + eps


def boundary_energy_func_trained(data, edges, model, extracted_feature_type):
    """Compute all energies using a trained model

    Parameters
    ----------
    data : np.array
        The image being segmented.
    edges : list
        Edges between all neighboring pixels in the image.
    model : model object
        The trained model object (e.g. sklearn.tree.tree.DecisionTreeClassifier)
    extracted_feature_type : {1}
        Type-1 is the "absolute difference, average" feature.
    """
    edge_vals = [(data[xi], data[xj]) for xi, xj in edges]
    edge_feature = common.extract_boundary_feature(extracted_feature_type, edge_vals)

    # Uses np.maximum() to avoid doing a log of zero.
    # Uses `[:, 1]` because predict_proba() returns probabilties for both
    # fg, bg classes.
    return np.abs(np.log(np.maximum(10 ** -5, model.predict_proba(edge_feature)[:, 1])))


def group_all_params(regional_model, boundary_model, regional_params,
                     boundary_params):
    """Group together all parameters

    Parameters
    ----------
    regional_model : dict
    boundary_model : dict
    regional_params : dict
    boundary_params : dict

    Returns
    -------
    all_params : dict
    """
    all_params = {
        'r_model':  regional_model.get('model_type'),
        'r_weight': regional_params.get('weight'),
        'r_gamma':  regional_params.get('gamma'),
        'b_model':  boundary_model.get('model_type'),
        'b_weight': boundary_params.get('weight'),
        'b_sigma':  boundary_params.get('sigma'),
        'b_gamma':  boundary_params.get('gamma'),
        'b_eps':    boundary_params.get('eps'),
        'b_feat':   boundary_model.get('feature_extractor_type')
    }

    return all_params


def segment(weighted_graph, image_shape):
    """Peforms a segmentation

    Uses the minimum cut algorithm in NetworkX.

    Parameters
    ----------
    weighted_graph : NetworkX graph
    image_shape : tuple

    Returns
    -------
    segmentation : np.array
        The segmented image, which has 0's for pixels belonging to the
        bg class and 1's for the fg class.
    cut_cost : int
        The cost of the segmentation.
    bg_nodes, fg_nodes : list
        The nodes that have been assigned to the bg, fg classes.
    """
    cut_cost, cut_nodes = nx.minimum_cut(weighted_graph, "s", "t", capacity='weight')
    bg_nodes, fg_nodes = cut_nodes

    segmentation = np.ones(image_shape)
    for node in bg_nodes:
        if node not in ['s','t']:
            segmentation[node] = 0

    return (segmentation, cut_cost, bg_nodes, fg_nodes)
