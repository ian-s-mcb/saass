"""Contains functionality for model creation and training
"""
import numpy as np
from sklearn.tree import DecisionTreeClassifier

from . import common


def create_regional_model(model_type='normal'):
    """Creates a regional model

    Currently, the only supported model type is 'normal'.

    Parameters
    ----------
    model_type : {'normal'}

    Returns
    -------
    regional_model : dict
    """
    # Handle unsupported arguments
    supported_model_types = ('normal',)
    if model_type not in supported_model_types:
        raise Exception('Unsupported regional model type: {}'.format(model_type))

    regional_model = {'model_type': model_type,
                      'is_trained': False}
    return regional_model


def create_boundary_model(model_type='exponential',
                          extracted_feature_type=None):
    """Creates a boundary model

    Supports several model types. Only the 'decision_tree' type allows for model
    training.

    Parameters
    ----------
    model_type : {'constant', 'exponential', 'inverse', 'sigmoidal', \
                  'decision_tree'}
    extracted_feature_type : {1}
        Type-1 is the "absolute difference, average" feature.

    Returns
    -------
    boundary_model : dict
    """
    # Handle unsupported arguments
    supported_model_types = ('constant', 'exponential', 'inverse', 'sigmoidal',
                             'decision_tree')
    if model_type not in supported_model_types:
        raise Exception('Unsupported boundary model type: {}'.format(model_type))

    if model_type == 'decision_tree':
        dtc_model = DecisionTreeClassifier(class_weight='balanced')
        boundary_model = {'model_type': model_type,
                          'model': dtc_model,
                          'is_trained': False,
                          'extracted_feature_type': extracted_feature_type}
    else:
        boundary_model = {'model_type': model_type,
                          'is_trained': False}
    return boundary_model


def train_regional_model(regional_model, training_data, training_labels):
    """Trains a regional model

    Parameters
    ----------
    regional_model : dict
    training_data : np.array
        Has either two or three axes. The first two axes specify a pixel
        location. If the image is multimodal, then the array has a third
        axis that specifies a mode/feature.
    training_labels : np.array
        Class labeling from a previous segmentation, which was either
        created manually with an image editor or create automatically
        though a first pass with SAASS. The array has two axes that
        specify a pixel location. The values in the array are label values
        (0 for bg, 1 for fg).

    Notes
    -----
    This function modifies the model dict that is provided as an argument.
    """
    # Handle unsupported arguments
    supported_model_types = ('normal',)
    if regional_model['model_type'] not in supported_model_types:
        raise Exception('Unsupported regional model type: {}'.format(model_type))

    bg = {'mean': training_data[training_labels == 0].mean(),
          'std' : training_data[training_labels == 0].std()}
    fg = {'mean': training_data[training_labels == 1.0].mean(),
          'std' : training_data[training_labels == 1.0].std()}

    regional_model['bg'] = bg
    regional_model['fg'] = fg
    regional_model['is_trained'] = True


def train_boundary_model(boundary_model, training_data, training_labels, edges):
    """Trains a boundary model

    Parameters
    ----------
    boundary_model : dict
    training_data : np.array
        Has either two or three axes. The first two axes specify a pixel
        location. If the image is multimodal, then the array has a third
        axis that specifies a mode/feature.
    training_labels : np.array
        Class labeling from a previous segmentation, which was either
        created manually with an image editor or create automatically
        though a first pass with SAASS. The array has two axes that
        specify a pixel location. The values in the array are label values
        (0 for bg, 1 for fg).
    edges : list of tuples
        Indices of neighboring pixels in the image. List consists of various
        2-component tuples. Each component is the index of a pixel. The two
        pixels in any given tuple are pixels that neighbor one another. This
        list provides a way to index into `training_data`. Typically, this list
        is obtained by creating a 2D grid graph of the dimensions of the image
        and retreiving all edges from that graph.

    Returns
    -------
    Xtrain : np.array
        The feature array that was built and used during training. This array
        can be used to visualize the trained model, but doing so is optional.

    Notes
    -----
    This function modifies the model dict that is provided as an argument.
    """
    # Handle unsupported arguments
    supported_model_types = ('decision_tree',)
    if boundary_model['model_type'] not in supported_model_types:
        raise Exception('Boundary model type {} ' \
                        'does not support training'.format(model_type))

    # Get the locations of all the bg, fg nodes
    # in the initial segmentation
    bg_nodes = set(zip(*((training_labels < training_labels.max()).nonzero())))
    fg_nodes = set(zip(*((training_labels > training_labels.min()).nonzero())))

    # Cast training data as float
    # Needed because the model misbehaves when given integer data
    training_data = training_data.astype(np.float)

    # Get values/weights for bg, fg, boundary edges
    # boundary edges are edges between nodes that belong to different classes
    # ie. (bg, fg) or (fg, bg)
    bg_edge_vals = [
        (training_data[xi], training_data[xj])
        for xi, xj in edges
        if ((xi in bg_nodes) and (xj in bg_nodes))]
    fg_edge_vals = [
        (training_data[xi], training_data[xj])
        for xi, xj in edges
        if ((xi in fg_nodes) and (xj in fg_nodes))]
    boundary_edge_vals = [
        (training_data[xi], training_data[xj])
        for xi, xj in edges
        if ((xi in bg_nodes) and (xj in fg_nodes) or
            (xi in fg_nodes) and (xj in bg_nodes))]

    # Compute extracted features for bg, fg, boundary edges values
    Xbg    = common.extract_boundary_feature(boundary_model['extracted_feature_type'], bg_edge_vals)
    Xfg    = common.extract_boundary_feature(boundary_model['extracted_feature_type'], fg_edge_vals)
    Xbound = common.extract_boundary_feature(boundary_model['extracted_feature_type'], boundary_edge_vals)

    # Stack feature arrays
    Xtrain = np.vstack([Xbg, Xfg, Xbound])
    Ytrain = np.hstack([np.zeros((Xbg.shape[0] + Xfg.shape[0],)),
                        np.ones((Xbound.shape[0],))])

    # Train/fit model with features
    boundary_model['model'].fit(Xtrain, Ytrain)
    boundary_model['is_trained'] = True

    return Xtrain
