"""Contains functionality needed for both training and segmenting
"""
import numpy as np


def extract_boundary_feature(extracted_feature_type, edge_vals):
    """Extracts a specified feature from boundary edge values

    Parameters
    ----------
    extracted_feature_type : {1}
        Type-1 is the "absolute difference, average" feature.
    edge_vals : list of tuples
        Values at the nodes that make up each boundary edge. List consists of
        one tuple for each edge, and each tuple has two components for the two
        nodes that make up an edge.

    Returns
    -------
    feature : np.array
    """
    feature_func = {1: extract_boundary_feature_type_1}

    if extracted_feature_type not in feature_func:
        raise Exception('Unsupported feature extractor type: {}'.format(extracted_feature_type))

    return feature_func[extracted_feature_type](edge_vals)


def extract_boundary_feature_type_1(edge_vals):
    """Extracts the type-1 feature from boundary edge values

    Type-1 is the "absolute difference, average" feature.

    Parameters
    ----------
    edge_vals : list of tuples
        Values at the nodes that make up each boundary edge. List consists of
        one tuple for each edge, and each tuple has two components for the two
        nodes that make up an edge.

    Returns
    -------
    feature : np.array
    """
    feature = np.array([(np.abs(v1 - v2), 0.5 * (v1 + v2))
                        for v1, v2 in edge_vals])

    return feature
