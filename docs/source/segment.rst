Segment
====================

.. automodule:: saass.segment
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:
