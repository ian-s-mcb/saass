Getting Started
==================

The SAASS library supports both Python 2.7 and Python 3.5.

Since the library hasn't been fully released and major codebase changes are
expected, we recommend installing the library in editable/developer mode, which
is demonstrated below. This mode allows revisions to be made to the library
without needing a reinstallation. Revisions can be made through either repo
activity (commit, pull, checkout, etc.) or uncommitted source code changes.

.. code-block:: bash

    git clone https://gitlab.com/ian-s-mcb/saass.git
    cd saass
    pip install -e .
