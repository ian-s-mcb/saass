.. saass documentation master file, created by
   sphinx-quickstart on Mon Jul 25 15:13:21 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to saass's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :glob:

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: API

   train
   segment
   common

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
