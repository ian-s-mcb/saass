# saass

A image segmentation library written in Python, powered by graph cuts.

## About

The name for this library comes from a medical imaging research project
called Semi-Automatic Adaptive Statistical Segmentation. The library
dates back to a 2016 summer research internship I had at [Memorial Sloan
Kettering Cancer Center][msk]. My assignment for that internship was to
reimplement an in-house, C++ algorithm for labeling tumor and non-tumor
cells.

The technolgies used in `saass` are Python, Numpy, Scikit,
NetworkX.

To see `saass` in action, check out the Jupyter notebooks in [the
examples repo][examples]. Here is the library [documentation][].

This library was presented on 2016-08-22, at a showcase of MSKCC
internship projects. Here are [the slides][slides] from that
presentation.

## Acknowledgements

The mentors for this project were:

* [Jason Yu-chi Hu][jason], AI specialist in medical imaging, MSKCC
* [Michael Grossberg][michael], Computer Science professor, CCNY

[documentation]: http://saass.readthedocs.io/en/latest/
[examples]: https://gitlab.com/ian-s-mcb/saass-examples
[jason]: https://www.linkedin.com/in/jason-yu-chi-hu-7a6679107
[michael]: http://www-cs.ccny.cuny.edu/~grossberg/index.html
[msk]: https://www.mskcc.org/
[slides]: msk_presentation.pdf
